import MovieRepository from '~/repositories/MovieRepository'

export default ($axios) => ({
    movie: MovieRepository($axios)
})
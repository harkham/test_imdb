const resource = 'http://www.omdbapi.com/?apikey=' + process.env.omdb_api_key
export default ($axios) => ({
  searchByname(title) {
    return $axios.get(`${resource}&s=${title}`)
  },
  getById(id) {
    return $axios.get(`${resource}&i=${id}`)
  },
})